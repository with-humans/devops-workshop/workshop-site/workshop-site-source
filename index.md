# Available Services

| Service             | URL                                                                     |  credentials   |
| ------------        | ----------------------------------------------------------------------- | -------------- |
| Grafana             | https://grafana.k3d.local.with-humans.org/                              | admin:password |
| Loki (for logcli)   | `https://loki.k3d.local.with-humans.org/`                               |                |
| ArgoCD              | https://argocd.k3d.local.with-humans.org/                               | admin:password |
| Argo Workflows      | https://argo-workflows.k3d.local.with-humans.org/                       |                |
| Argo Rollouts       | https://argo-rollouts.k3d.local.with-humans.org/                        |                |
| Prometheus          | https://prometheus.k3d.local.with-humans.org/                           |                |
| Alertmanager        | https://alertmanager.k3d.local.with-humans.org/                         |                |
| Prometheus Blackbox | https://prometheus-blackbox.k3d.local.with-humans.org/                  |                |
| Traefik Dashboard   | https://traefik-dashboard.k3d.local.with-humans.org/dashboard/          |                |
| Docker Registry     | `https://registry.k3d.local.with-humans.org:5003/`                      |                |
