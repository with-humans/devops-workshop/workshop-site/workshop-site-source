import { Application, marked, Router, sanitizeHtml } from "./deps.ts";

const app = new Application();
let READY = true;

const router = new Router();

router.get("/", async (ctx) => {
  const markdown = await Deno.readTextFile("index.md");
  const html = marked.parse(markdown);
  const safeHtml = sanitizeHtml(html);
  ctx.response.body = `
  <html>
  <body>
    ${safeHtml}
  </body>
  </html>
  `;
  ctx.response.status = 200;
  return ctx;
});

router.get("/ready", (ctx) => {
  ctx.response.status = READY ? 200 : 503;
  ctx.response.body = READY;
});

router.get("/live", (ctx) => {
  ctx.response.status = 200;
  ctx.response.body = true;
});

app.use(router.routes());
app.use(router.allowedMethods());

if (import.meta.main) {
  const controller = new AbortController();
  Deno.addSignalListener("SIGINT", () => {
    console.log("SIGINT");
    READY = false;
    controller.abort();
  });
  Deno.addSignalListener("SIGTERM", () => {
    console.log("SIGTERM");
    READY = false;
    controller.abort();
  });

  await app.listen({ port: 8080, signal: controller.signal });
}
